# Lab: Spring Boot - Primefaces

-----------------------------------------

This test project in Java has the sole purpose of integrating [Spring Boot](http://projects.spring.io/spring-boot/) or any of its derivatives and [Primefaces](http://www.primefaces.org/) (JSF).